# elm-model-example

Quick example of one way to model single source of truth in an Elm Single Page App where multiple pages share data.

# Install

`yarn install` or `npm install`

# Run

`yarn start` or `npm start`

# Example urls

See the pages using these links

[Tupac is a good dog](http://localhost:9071/dog/Tupac)        
[Biggie is a good dog](http://localhost:9071/dog/BiggieSmalls)      
[WuTang is a not found :(](http://localhost:9071/dog/WuTang)     
