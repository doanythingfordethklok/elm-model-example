module Models exposing (Dog, Page, PageModel, Person, User(..))


type alias Person =
    { name : String
    , email : String
    }


type alias Dog =
    { breed : String
    , name : String
    }


type User
    = Authenticating
    | Authenticated Person
    | Guest


type alias PageModel p =
    { p
        | user : User
        , dogs : List Dog
    }


type alias Page =
    { user : User
    , dogs : List Dog
    }
