module Main exposing (main)

import Browser exposing (Document, UrlRequest(..), application)
import Browser.Navigation as Navigation
import Html exposing (Html, div)
import Html.Attributes exposing (class, id)
import Json.Decode exposing (Value)
import Models exposing (..)
import Pages.Dog as DogPage
import Pages.NotFound as NotFoundPage
import Routing
import Url exposing (Url)
import Url.Builder exposing (absolute)


type PageData
    = DogData (PageModel DogPage.Model)
    | NotFoundData (PageModel NotFoundPage.Model)


type alias Model =
    { page : PageData
    , url : Url
    , nav_key : Navigation.Key
    }


type Msg
    = UrlRequested UrlRequest
    | UrlChanged Url
    | DogMessage DogPage.Msg
    | NotFoundMessage NotFoundPage.Msg


getPageData : PageData -> Page
getPageData data =
    case data of
        DogData cached ->
            { user = cached.user
            , dogs = cached.dogs
            }

        NotFoundData cached ->
            { user = cached.user
            , dogs = cached.dogs
            }


locationChange : Model -> ( Model, Cmd Msg )
locationChange model =
    let
        route =
            Routing.matchUrl model.url
    in
    case route of
        Routing.DogRoute name ->
            let
                ( mod, cmd ) =
                    DogPage.init name <|
                        getPageData model.page
            in
            ( { model | page = DogData mod }, Cmd.map DogMessage cmd )

        Routing.NotFoundRoute ->
            let
                ( mod, cmd ) =
                    NotFoundPage.init (Url.toString model.url) <|
                        getPageData model.page
            in
            ( { model | page = NotFoundData mod }, Cmd.map NotFoundMessage cmd )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UrlChanged url ->
            locationChange { model | url = url }

        UrlRequested url_request ->
            case url_request of
                Internal url ->
                    ( model, Navigation.pushUrl model.nav_key <| Url.toString url )

                External href ->
                    ( model, Navigation.load href )

        NotFoundMessage submsg ->
            case model.page of
                NotFoundData data ->
                    let
                        ( page, cmd ) =
                            NotFoundPage.update submsg data
                    in
                    ( { model | page = NotFoundData page }, Cmd.map NotFoundMessage cmd )

                _ ->
                    ( model, Cmd.none )

        DogMessage submsg ->
            case model.page of
                DogData data ->
                    let
                        ( page, cmd ) =
                            DogPage.update submsg data
                    in
                    ( { model | page = DogData page }, Cmd.map DogMessage cmd )

                _ ->
                    ( model, Cmd.none )


initialPage : Page
initialPage =
    Page
        Guest
        [ Dog "Rottweiler" "Tupac", Dog "Bullog" "BiggieSmalls" ]


init : flags -> Url -> Navigation.Key -> ( Model, Cmd Msg )
init _ url nav_key =
    let
        ( page, cmd ) =
            NotFoundPage.init (Url.toString url) initialPage

        model =
            { url = url
            , nav_key = nav_key
            , page = NotFoundData page
            }
    in
    locationChange model


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


viewPage : Model -> Html Msg
viewPage model =
    case model.page of
        DogData mod ->
            Html.map DogMessage (DogPage.view mod)

        NotFoundData mod ->
            Html.map NotFoundMessage (NotFoundPage.view mod)


view : Model -> Document Msg
view model =
    { title = "Demo"
    , body =
        [ div [ id "root" ]
            [ viewPage model
            ]
        ]
    }



-- start the program


main : Program Value Model Msg
main =
    application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = UrlRequested
        , onUrlChange = UrlChanged
        }
