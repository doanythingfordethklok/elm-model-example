module Routing exposing (Route(..), matchUrl, matchers)

import Url exposing (Url)
import Url.Parser exposing ((</>), (<?>), Parser, map, oneOf, parse, s, string)


type Route
    = DogRoute String
    | NotFoundRoute


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map DogRoute (s "dog" </> string)
        ]


matchUrl : Url -> Route
matchUrl url =
    case parse matchers url of
        Just route ->
            route

        Nothing ->
            NotFoundRoute
