module Pages.Dog exposing (Model, Msg, init, update, view)

import Browser.Navigation as Navigation
import Html exposing (..)
import Html.Attributes exposing (..)
import Models exposing (..)
import Url.Builder exposing (absolute)


type Msg
    = Noop


type alias Model =
    { dog : Dog
    , err : Maybe String
    }


init : String -> Page -> ( PageModel Model, Cmd Msg )
init name cached =
    let
        m_dog =
            List.filter (\d -> d.name == name) cached.dogs
                |> List.head
    in
    case m_dog of
        Just dog ->
            ( { dogs = cached.dogs
              , user = cached.user
              , dog = dog
              , err = Nothing
              }
            , Cmd.none
            )

        Nothing ->
            ( { dogs = cached.dogs
              , user = cached.user
              , dog = Dog "" ""
              , err = Just <| name ++ " not found"
              }
            , Cmd.none
            )


update : Msg -> PageModel Model -> ( PageModel Model, Cmd Msg )
update action model =
    case action of
        Noop ->
            ( model, Cmd.none )


viewDog : Dog -> Html Msg
viewDog dog =
    li []
        [ a [ href <| absolute [ "dog", dog.name ] [] ] [ text dog.name ]
        ]


view : PageModel Model -> Html Msg
view { dogs, dog, err } =
    case err of
        Just str ->
            div []
                [ h3 [] [ text <| "ERROR: " ++ str ]
                , p [] [ text "Maybe you want one of these good dogs?" ]
                , ul [] (List.map viewDog dogs)
                ]

        Nothing ->
            div []
                [ div [] [ text dog.name ]
                , div [] [ text dog.breed ]
                ]
