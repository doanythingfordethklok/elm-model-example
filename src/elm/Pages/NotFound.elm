module Pages.NotFound exposing (Model, Msg, init, update, view)

import Html exposing (Html, div, text)
import Models exposing (..)


type Msg
    = Noop


type alias Model =
    { url : String
    }


init : String -> Page -> ( PageModel Model, Cmd Msg )
init url cached =
    ( { user = cached.user
      , dogs = cached.dogs
      , url = url
      }
    , Cmd.none
    )


update : Msg -> PageModel Model -> ( PageModel Model, Cmd msg )
update action model =
    case action of
        Noop ->
            ( model, Cmd.none )


view : PageModel Model -> Html Msg
view { url } =
    div [] [ text <| "NOT FOUND: " ++ url ]
