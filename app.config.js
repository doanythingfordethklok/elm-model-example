const path = require('path');
const env = require(process.env.APP_CONFIG || './local.env.json');
const public_path = '';

module.exports = (out = path.join(process.cwd(), 'dist')) => {
  const static_target = path.resolve(out);
  const target = path.join(static_target, 'static');
  const static_resources_path = path.join(public_path, 'static');

  return {
    env,
    static_target,
    public_path: public_path,
    html_steps: [
      {
        entry: path.resolve('./src/static/index.html'),
        target: static_target,
        name: 'index'
      }
    ],
    build_steps: [
      {
        type: 'elm',
        options: {
          inline: true,
          entry: path.resolve('./src/elm/Main.elm'),
          name: 'main',
          target,
          public_path: static_resources_path,
          optimize: env.auto_recompile !== true,
          debug: env.auto_recompile === true
        }
      },
      {
        type: 'js',
        options: {
          inline: true,
          entry: path.resolve('./src/static/index.js'),
          name: 'index',
          target,
          public_path: static_resources_path
        }
      }
    ]
  };
};
