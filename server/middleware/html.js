const fs = require('fs');
const path = require('path');

module.exports.auto_recompile = ({ build_steps, html_steps }) => {
  const { devMiddleware } = require('ms-pacman');
  const watches = [ path.resolve(process.cwd(), 'src') ];

  return devMiddleware(build_steps, html_steps, watches);
};

module.exports.precompiled = ({ static_target }) => {
  const INDEX_HTML = fs.readFileSync(path.join(static_target, 'index.html'), 'utf-8');

  return (req, res, next) => {
    res.set('content-type', 'text/html');
    res.send(INDEX_HTML);
    res.end();
  };
};
