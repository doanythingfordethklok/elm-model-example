const express = require('express');
const path = require('path');
const html = require('./middleware/html.js');
const { env, build_steps, html_steps } = require('../app.config.js')();
const { port, auto_recompile } = env;
const server = express();

if (auto_recompile) {
  console.log('auto_recompile is enabled.');
  server.get(`/*`, html.auto_recompile({ build_steps, html_steps }));
}
else {
  console.log(`/static`);
  server.use(`/static`, express.static(path.join(static_target, 'static')));
  server.get(`/*`, html.precompiled({ static_target }));
}

server.use(function(err, req, res, next) {
  res.status(500).send(err.message);
});

server.listen(port);
console.log(`Application is now running at http://localhost:${port}`);
