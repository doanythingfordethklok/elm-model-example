const path = require('path');
const argv = require('yargs').argv;
const { buildAll, writeFilesToFileSystem } = require('ms-pacman');

if (!argv.target) {
  console.log('argv.target is required');
  process.exit(1);
}
const { build_steps, html_steps } = require('../app.config.js')(path.join(process.cwd(), argv.target));

const optimized_build_steps = build_steps.map((step) => {
  if (step.type === 'elm') {
    return {
      ...step,
      options: {
        ...step.options,
        optimize: true,
        debug: false
      }
    };
  }

  return step;
});

buildAll(optimized_build_steps, html_steps)
  .then((files) => {
    files.forEach((f) => console.log({ ...f, src: '[...]' }));

    return writeFilesToFileSystem(files);
  })
  .then(() => {
    console.log('Congratulations! Make was successful.');
    process.exit();
  })
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });
